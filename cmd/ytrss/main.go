package main

import (
	"fmt"
	"io"
	"net/http"
	"os"

	"golang.org/x/net/html"
)

func errExit(args ...interface{}) {
	fmt.Println(args...)
	os.Exit(1)
}

const usage = `Usage: ytrss <url>

example: ytrss https://www.youtube.com/c/SebastianLague

if you are having questions or found a bug create an issue over at https://gitlab.com/Simerax/ytrss
`

func main() {
	if len(os.Args) < 2 || os.Args[1] == "--help" || os.Args[1] == "-h" {
		fmt.Print(usage)
		os.Exit(1)
	}
	url := os.Args[1]
	resp, err := http.Get(url)
	if err != nil {
		errExit("Unable to GET url:", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		errExit("Unexpected Status Code:", resp.StatusCode, "- expected 200")
	}

	t := html.NewTokenizer(resp.Body)

loop:
	for {
		tokenType := t.Next()

		switch tokenType {
		case html.ErrorToken:
			if t.Err() == io.EOF {
				break loop
			}
			errExit("Unable to parse html content:", t.Err())
		case html.StartTagToken, html.SelfClosingTagToken:
			tagName, hasAttrs := t.TagName()
			if string(tagName) == "link" && hasAttrs {
				href := ""
				isRSSTag := false
				more := true
				var keyBytes, valBytes []byte

				for more {
					keyBytes, valBytes, more = t.TagAttr()
					key := string(keyBytes)
					val := string(valBytes)

					// to know wheter or not this <link/> is acutally the wanted RSS link
					// we lookup type and title if at least one of them fits we can be confident
					// that its the correct link
					if key == "type" && val == "application/rss+xml" {
						isRSSTag = true
					}
					if key == "title" && val == "RSS" {
						isRSSTag = true
					}

					if key == "href" {
						href = val
					}
					if isRSSTag && href != "" {
						fmt.Println("Found link:", href)
						os.Exit(0) // no reason to continue parsing we got what we want
					}
				}

			}
		}
	}
	errExit("Unable to find RSS link")
}
