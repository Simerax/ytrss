# YTRSS - Get RSS Links from Youtube Channels

Simple tool to get the RSS Feed of a youtube channel.  
Install with: `go install gitlab.com/Simerax/ytrss/cmd/ytrss@latest`  
Run: `ytrss <url>`

### what?
In case you did not know every youtube channel has an RSS feed. 
You can use that feed to get an update in your favorite RSS reader when that channel uploads :)
